class WgConfigParser
  def initialize(config)
    # configuration should be given as
    # non-empty string
    # ensure that
    raise ArgumentError, 'Configuration must be a string' unless config.is_a? String
    raise ArgumentError, 'Configuration must not be empty' if config.strip.empty?

    # save configuration file content
    @config = config
  end

  def parse!
    # Parse configuration file
    # Do not validate the entries!

    # Ensure that a configuration is given
    raise 'Need configuration to parse.' unless @config

    section = nil
    res = {}

    @config.each_line do |line|
      if (new_section = /^\s*\[([A-Z][a-z]{3,8})\]\s*$/.match(line))
        # New section
        # Kind of [SectionName]
        # Minimum name length: 4
        # Maximum name length: 9
        # Must begin with a uppercase

        # For simplicity everything is written in lower case
        section = new_section[1].downcase

        # Sections can appear doubbe (e. g. for multiple peers)
        # Create empty section information
        # if section is seen the first time
        res[section] = [] unless res.key?(section)

        res[section] << {}
      elsif (key_value_pair = /^\s*([A-Z][A-Za-z]*)\s*=\s*(.*)\s*$/.match(line))
        # Key value pair detected
        # Must begin with a upper case
        # Must be a minimum of one
        # Whitespaces are ignored

        # Ensure we are in a section
        raise 'Key-Value-Pair outside a section.' unless section

        # For simplicity everything is written in lower case
        key = key_value_pair[1].downcase

        # Value could be case-senstive
        # Do not change it, just remove whitespaces
        value = key_value_pair[2].strip

        # Save key value pair
        res[section][-1][key] = [] unless res[section][-1].key? key
        res[section][-1][key] << value
      elsif /^\s*#.*$/.match?(line)
        # comment
        # do nothing
      elsif line.strip.empty?
        # empty line
        # do nothing
      else
        # Unknown statment detected
        # Maybe too strict regex?
        # Feel free to open a issue!
        raise "Invalid statement: #{line.dump}"
      end
    end

    # Save parsed configuration file
    @result = res

    return self
  end

  def validate!
    # Ensure valid configuration exist
    raise 'Configuration not parsed.' unless @result
    raise 'Configuration is empty.' if @result.keys.empty?

    # Interface section
    # Ensure there **only** one
    raise 'Missing interface section.' unless @result.key? 'interface'
    raise 'Missing interface configuration.' if @result['interface'].empty?
    raise 'Only one interface configuration is allowed.' if @result['interface'].length > 1

    # Ensure only known keys are used
    unless (@result['interface'][-1].keys - %w[privatekey postup postdown preup predown table mtu netns]).empty?
        raise 'Invalid key'
    end

    # Ensure there is **one** non-empty private key
    raise 'Missing private key.' unless @result['interface'][-1].key? 'privatekey'
    raise 'Only one private key is allowed.' if @result['interface'][-1]['privatekey'].length > 1
    raise 'Empty private key configured.' if @result['interface'][-1]['privatekey'][-1].empty?

    # Ensure there is **one** non-empty netns
    raise 'Missing netns.' unless @result['interface'][-1].key? 'netns'
    raise 'Only one netns is allowed.' if @result['interface'][-1]['netns'].length > 1
    raise 'Empty netns configured.' if @result['interface'][-1]['netns'][-1].empty?

    # Users may specify a table
    if @result['interface'][-1].key? 'table'
      # In this case only **one** is allowed
      raise 'Table are only allowed once.' if @result['interface'][-1]['table'].length > 1
      raise 'Invalid value for table' unless %w[off auto].include? @result['interface'][-1]['table'][-1].strip
    end

    # For a useful wireguard configuration
    # there must be at least one peer
    # WireGuard also works without peers <- we ignore this case
    raise 'No peers configured.' unless @result.key? 'peer'

    # Validate each peer
    @result['peer'].each do |peer|
      # Ensure only known keys are used
      unless (peer.keys - %w[publickey presharedkey endpoint allowedips persistentkeepalive]).empty?
          raise 'Invalid key'
      end

      # Ensure there is **one** non-empty public configured.
      raise 'No public key configured.' unless peer.key? 'publickey'
      raise 'Multiple public key configured.' if peer['publickey'].length > 1
      raise 'Empty public key configured.' if peer['publickey'][-1].strip.empty?

      # If a PSK is configured
      # Only **one** non-empty one is allowed
      if peer['presharedkey']
        raise 'PSK configured twice.' if peer['presharedkey'].length > 1
        raise 'Empty PSK configured.' if peer['presharedkey'][-1].strip.empty?
      end

      # Same for endpoint
      if peer['endpoint']
        raise 'Endpoint configured twice.' if peer['endpoint'].length > 1
        raise 'Empty endpoint configured.' if peer['endpoint'][-1].strip.empty?

      end

      # A peer need at least one allowed IP
      raise 'No Allowed IPs configured.' unless peer.key? 'allowedips'
      # Ensure noone is empty
      peer['allowedips'].each do |ip|
        raise 'Empty Allowed IP configured.' if ip.strip.empty?
      end
    end

    return self
  end

  def extract_netns
    raise 'No configuration parsed.' unless @result

    return @result['Interface'][-1]['netns']
  end

  def full_configuration
    raise 'No configuration parsed.' unless @result

    return @result
  end

  def private_key
    raise 'No configuration parsed.' unless @result

    return @result['interface'][-1]['privatekey']
  end

  def mtu
    raise 'No configuration parsed.' unless @result

    if @result['interface'][-1].key? 'mtu'
      return @result['interface'][-1]['mtu']
    else
      return '1420'
    end
  end

  def postup
    raise 'No configuration parsed.' unless @result

    return @result['interface'][-1]['postup'].to_a
  end

  def postdown
    raise 'No configuration parsed.' unless @result

    return @result['interface'][-1]['postdown'].to_a
  end

  def preup
    raise 'No configuration parsed.' unless @result

    return @result['interface'][-1]['preup'].to_a
  end

  def predown
    raise 'No configuration parsed.' unless @result

    return @result['interface'][-1]['predown'].to_a
  end

  def table?
    raise 'No configuration parsed.' unless @result

    return @result['interface'][-1]['table'][-1] != 'off'
  end

  def peers
    raise 'No configuration parsed.' unless @result

    res = []
    @result['peer'].each do |peer|
      res << {}
      res[-1][:publickey] = peer['publickey'][-1]
      res[-1][:psk] = peer['presharedkey'][-1] if peer.key? 'presharedkey'
      res[-1][:endpoint] = peer['endpoint'][-1] if peer.key? 'endpoint'
      # Split multiple allowed IPs in one statement into
      # a array (val.split), and merge it to the other ones (flatten)
      # ensure there are no whitespaces (map(&:strip))
      # delete empty ones (delete_if(&:empty?))
      res[-1][:allowedips] = (peer['allowedips'].map { |val| val.split(',') }).flatten.map(&:strip).delete_if(&:empty?)
      res[-1][:persistent_keepalive] = peer['persistentkeepalive'][-1] if peer.key? 'persistentkeepalive'
    end

    return res
  end
end

wg_parser = WgConfigParser.new(File.read('config.conf')).parse!.validate!
